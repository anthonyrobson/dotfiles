;;; init.el -*- lexical-binding: t; -*-

(setq gc-cons-threshold most-positive-fixnum
      inhibit-startup-screen t
      initial-scratch-message nil)

(scroll-bar-mode -1)
(tool-bar-mode -1)
(tooltip-mode -1)
(fset 'display-startup-echo-area-message #'ignore)

(require 'config-base (expand-file-name "config/config-base" user-emacs-directory))

(require 'module-tex)

(add-hook 'window-setup-hook (lambda () (setq gc-cons-threshold (* 50 1024 1024))))

;;; init.el ends here
