;;; config-ui.el

(setq
 mouse-wheel-progressive-speed nil
 mouse-wheel-scroll-amount '(1 ((shift) . 1) ((control) . nil))
 ns-pop-up-frames nil
 )

(provide 'config-ui)
;;; config-ui.el ends here
